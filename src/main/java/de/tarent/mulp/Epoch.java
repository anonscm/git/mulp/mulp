package de.tarent.mulp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;

public class Epoch {
	
	protected final static Logger logger = Logger.getLogger(Epoch.class.getName());

	private int inputCount;
	private int outputCount;
	private int count;
	private List<List<Double>> inputs = new ArrayList();
	private List<List<Double>> teachers = new ArrayList();
	private List<Integer> order;
	
	public Epoch(){
		
	}

	public void read(String filename){
		File file = new File(filename);
		read(file);
	}

	public void read(File file){
		order = new ArrayList<Integer>();
		try {
			FileInputStream stream = new FileInputStream(file);
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

			String line = reader.readLine();
			StringTokenizer tokenizer;
			
			int index=0;
			while(line!=null){
				
				//comments and whitespace
				if ((line.startsWith("#")) || (line.trim().equals(""))){
					line=reader.readLine();
					continue;
				}
				tokenizer = new StringTokenizer(line);
				inputs.add(new ArrayList<Double>());
				teachers.add(new ArrayList<Double>());
				
				Boolean input = true;
				while(tokenizer.hasMoreTokens()){
					String token = tokenizer.nextToken();
					if (token.equals(":")){
						input = false;
						continue;
					}
					Double value = Double.valueOf(token);
					
					if (input){
						//System.out.println("new input value: "+value);
						inputs.get(index).add(value);
					} else {
						//System.out.println("new teacher value: "+value);
						teachers.get(index).add(value);
					}
				}
				order.add(index);
				index++;
				line=reader.readLine();
			}
			count=index;
			//System.out.println(index);

		} catch (FileNotFoundException e) {
			logger.warning("epoche file not found: "+e.getMessage());
		} catch (IOException e) {
			logger.warning("IOException while trying to read the epoche file: "+e.getMessage());
		}


	}
	
	public List<Double> getInput(int index){
		return inputs.get(order.get(index));
	}
	
	public List<Double> getTeacher(int index){
		return teachers.get(order.get(index));
	}
	
	public int getCount(){
		return count;
	}
	
	public void shuffle(){
		Collections.shuffle(order);
	}
	
	public int getInputSize(){
		return inputCount;
	}
	
	public int getOutputSize(){
		return outputCount;
	}

}
