package de.tarent.mulp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Logger;

import de.tarent.mulp.config.Config;
import de.tarent.mulp.config.NetProperties;

/**
 * This is the central class of the neural network project, the multi layer perceptron (mlp) itself.
 * The structure is limited to three layers (one input one hidden and one output layer). The number
 * of neurons of each layer can be configured just like the learning rates and training steps.
 * 
 * @author Steffi Tinder, tarent GmbH
 *
 */

public class MLP {

	protected final static Logger logger = Logger.getLogger(MLP.class.getName());

	private Weights[] weights = new Weights[3];
	private List<List<Double>> layerInput = new ArrayList();
	private List<Double> output = new ArrayList<Double>();
	private List<Double> eta = new ArrayList<Double>();
	private int[] structure = new int[3];
	private List<List<Double>> delta = new ArrayList();
	private Epoch epoch;

	public MLP(int a,int b,int c){

		structure[0]=a;
		structure[1]=b;
		structure[2]=c;

		weights[0]=new Weights(a+1,b);
		weights[1]=new Weights(b+1,c);

		for (int i=0;i<2;i++) weights[i].initializeWithRandomNumber(); 

		layerInput.add(new ArrayList<Double>());
		layerInput.add(new ArrayList<Double>());
		Utils.initialize(layerInput.get(0),structure[0]+1,1);
		Utils.initialize(layerInput.get(1),structure[1]+1,1);

		delta.add(new ArrayList<Double>());
		delta.add(new ArrayList<Double>());
		delta.add(new ArrayList<Double>());


		initializeLearningRates();

		Utils.initialize(output,structure[2],0);

		/* Layer Input Matrix erstellen (bias!) */

		/*Die Inputs einer Schicht sind alle gleich den Ausgängen der vorherigen Schicht. 
		Daher braucht pro Schicht nur ein Vektor mit den errechneten Inputs gespeichert werden.
		Der 0te Vektor ist der Input, den das erste Hidden Layer erhält.*/


	}

	public boolean checkEpoche(Epoch e){
		//TODO: implement
		return true;
	}

	private void initializeLearningRates(){
		//die Lernraten für die einzelnen Schichten setzen
		//Standardwerte:
		//Output 0.01 ... 0.1
		//Hidden 0.1  ... 0.5

		//nicht andersherum?

//		eta.add(0.01);
//		eta.add(0.001);


		eta.add(0.001);
		eta.add(0.01);

	}

	public void setHiddenLearningRate(double eta){
		this.eta.set(0,eta);
	}

	public void setOutputLearningRate(double eta){
		this.eta.set(1, eta);
	}

	public List<Double> compute(List<Double> input){
		return forward(input);
	}

	public List<Double> forward(List<Double> input){  //führt den Vorwärtsschritt durch
		double net=0;

		for(int schicht=0; schicht<3; schicht++){   //für alle Schichten (inkl. der Inputschicht, die den Input nicht ver�ndert)
			for(int neuronIndex=0;neuronIndex<structure[schicht];neuronIndex++){  //für alle Neuronen in der Schicht i
				if (schicht==0){  // erste Schicht verändert den Input nicht
					layerInput.get(schicht).set(neuronIndex+1,input.get(neuronIndex));
					// wegen bias geändert (+1)
				} 
				else{ 
					net=0;
					for(int eingang=0;eingang<structure[schicht-1]+1;eingang++){  // net berechnen, bias nicht vergessen

						net=net+weights[schicht-1].get(eingang, neuronIndex)*layerInput.get(schicht-1).get(eingang);
					} //eingang
					// net ist jetzt berechnet
					if (schicht==2){// wenns die letzte Schicht ist, ab in den output
						//zum testen mal die step funktion
						output.set(neuronIndex, Math.tanh(net));
						
//						if (net>=0) output.set(neuronIndex, 0.9); 
//						else output.set(neuronIndex, -0.9);
						
					} 
					else { //ansonsten ist der output gleichzeitig der input der nächsten schicht
						
						layerInput.get(schicht).set(neuronIndex+1, Math.tanh(net)); // bias nicht Überschreiben! (daher: neuron_index+1)
						
//						if (net>=0) layerInput.get(schicht).set(neuronIndex +1, 0.9); 
//						else layerInput.get(schicht).set(neuronIndex +1 , -0.9);
						
						/*Das Bias Neuron errechnet zwar einen Output (also einen Input für die nächste Schicht) 
						erhält aber selbst stets den konstanten Input 1, der an dieser Stelle natürlich nicht überschrieben werden darf*/
					}
				} //else nicht erste schicht
			}//for alle neuronen
		}//for alle schichten
		return output;
	}

	public void backward(List<Double> teacher){  // backpropagation, die Veränderungen werden in delta gespeichert
		for (int i=0;i<2;i++){
			//für jedes Neuron ab dem 1. Hidden Layer gibt es ein delta
			Utils.initialize(delta.get(i), structure[i+1], 0);
		}	
		for (int schicht=2;schicht>0;schicht--){ //Die Schichten werden von hinten nach vorne durchgezählt
			for(int neuron=0;neuron<structure[schicht];neuron++){
				if (schicht==2){ // wenn es die letzte Schicht ist
					//ein bisschen deutlicher:
					//Fehlersignal:
					double sigma = (1-output.get(neuron)*output.get(neuron))*(teacher.get(neuron)-output.get(neuron));
					delta.get(schicht-1).set(neuron,sigma);
					// hier wird die Deltaregel angewendet
				}
				else{// ansonsten werden die deltas rekursiv mithilfe der schon berechneten deltas berechnet 
					double sum=0;
					for (int ausgang=0;ausgang<structure[schicht+1];ausgang++){
						sum=sum+delta.get(schicht).get(ausgang)*weights[schicht].get(neuron+1,ausgang);//+1, weil bei der gebenden Schicht das 0te Neuron immer das bias neuron ist
						// hier wird die Summe der deltas der Nachfolgeneuronen gebildet
					}
					double sigma = (1-layerInput.get(schicht).get(neuron+1)*layerInput.get(schicht).get(neuron+1))*sum;
					delta.get(schicht-1).set(neuron,sigma);
				}//else letzte Schicht?
			}//for alle neuronen der schicht;
		}//for alle schichten
	}

	// get_fault berechnet den Fehler des aktuellen Musters und gibt diesen als double-Wert zurück
	public double getFault(List<Double> teacher){
		double fault=0;
		for (int i=0;i<teacher.size();i++){
			//)o(i)-t(i))^2, oder nicht?
			fault=fault+((output.get(i)-teacher.get(i))*(output.get(i)-teacher.get(i))); 
			//die Fehler der einzelnen Ergebnisse werden aufsummiert
		}
		return fault;
	}

	public void update(){// mlp::update führt die Gewichtsveränderungen durch, die im backward-Schritt berechnet wurden
		Double newValue;
		for (int schicht=1;schicht<3;schicht++){ //für die Neuronen der Input-Schicht sind keine deltas gespeichert
			for(int eingang=0;eingang<structure[schicht-1]+1;eingang++){ //für alle gewichte am neuron inkl. bias
				for (int neuron=0;neuron<structure[schicht];neuron++){  // für alle neuronen in der schicht
					newValue=weights[schicht-1].get(eingang, neuron)+eta.get(schicht-1)*layerInput.get(schicht-1).get(eingang)*delta.get(schicht-1).get(neuron);
					weights[schicht-1].set(eingang, neuron,newValue);
					//hier wird auf die Berechnungen des backward-Schrittes zurückgegriffen um die Gewichtsveränderungen durchzuführen
				}
			}
		}
	}

	public Weights[] getWeights() {
		return weights;
	}

	public void setWeights(Weights[] weights) {
		this.weights = weights;
	}

	public double run(Epoch epoche, boolean verbose, boolean printfault){ // das Netz wird nacheinander mit allen Mustern einer Epoche trainiert
		double fault=0;
		for (int i=0;i<epoche.getCount();i++){ // einmal die ganze Epoche durch, count ist die Anzahl der Muster

			if (verbose){
				System.out.print("forward: ");
				Utils.printList(epoche.getInput(i));

				System.out.print("Teacher: ");
				Utils.printList(epoche.getTeacher(i));
			}
			//forward step
			List<Double> output= forward(epoche.getInput(i));

			if (verbose){
				System.out.print("Ouput: ");
				Utils.printList(output);
			}
			//backward step and learning
			backward(epoche.getTeacher(i));
			update(); // die Gewichte werden aktualisiert
			//epoche fault
			fault=fault+Math.abs(getFault(epoche.getTeacher(i))); // Epochenfehler wird berechnet
		}
	return fault;
	}

	public void runTraining(Epoch e, int steps, boolean verbose, boolean printfaults, BufferedWriter writer){
		for (int i=0;i<steps;i++){
			double fault=run(e,verbose,printfaults);
			e.shuffle();

			if (printfaults){
				if (verbose){
					System.out.print("epoche fault: ");
				}
				System.out.println(fault);
			}

			if (writer!=null){
				try {
					writer.write(String.valueOf(fault));
					writer.newLine();
				} catch (IOException e1) {
					logger.info("Problems occured while trying to write fault File: "+e1.getMessage());
				}
			}
		}
	}

	public void runTest(Epoch epoche){
		/*das Netz führt nacheinander mit allen Mustern einer Epoche 
		den Värwärtsschritt durch, ohne das etwas gelernt wird*/
		// einmal durch die gesamte Epoche e
		for (int i=0;i<epoche.getCount();i++){
			System.out.print("forward: ");
			Utils.printList(epoche.getInput(i));
			System.out.print("Teacher: ");
			Utils.printList(epoche.getTeacher(i));
			forward(epoche.getInput(i)); //der Output wird berechnet
			System.out.print("Ouput: ");
			Utils.printList(output);
		} // für alle muster
	} // end of run_test 


	public void printStructure(){
		System.out.println("Struktur des Netzes:");
		for (int i=0;i<structure.length;i++){
			System.out.print(structure[i]+" ");
		}
	}

	//TODO: noch nicht getestet
	public void saveMlp(String filename) throws IOException{
		// die Gewichte werden in der Datei gespeichert, deren Namen in file gespeichert ist
		File file = new File(filename);
		Writer w = new FileWriter(file);
		BufferedWriter writer = new BufferedWriter(w);
		double v;
		//	target.open(file, ofstream::out);

		//for (int s=0;s<structure.size();s++) target<<structure[s]<<" ";
		for (int s=0;s<structure.length;s++){
			writer.write(structure[s]+" ");
			writer.newLine();
		}

		
		writer.newLine();

		for(int i=0;i<structure.length-1;i++){ //erste Dimension gibt an welche Gewichtsmatrix gemeint ist
			for (int j=0;j<structure[i]+1;j++){ // zweite Dimension ist so groß wie die "von" - Schicht + bias
				for (int k=0;k<structure[i+1];k++){ //die dritte Dimension ist so groß wie die "zu" Schicht
					//v=weights[i][j][k];
					writer.write(String.valueOf(weights[i].get(j, k)));
					writer.newLine();

					//fcvt

					//target << v << "\n";
				}
			}
		}
		writer.close();
		logger.info("saved successfully: "+filename);
	}

	//TODO: noch nicht getestet
	public void loadMlp(String filename){

		File file = new File(filename);
		FileInputStream stream;
		try {
			stream = new FileInputStream(file);
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			double v;
			String string;
			boolean exit=false;

			for (int s=0;s<structure.length;s++){
				string=reader.readLine();
				v=Double.valueOf(string);

				if (v!=structure[s])
				{
					System.out.println("Datei passt nicht zur aktuellen Netzstruktur!");
					printStructure();
					reader.close();
					exit=true;
					break;
				}
			}

			if (!exit){
				
					
				for(int i=0;i<structure.length-1;i++){ //erste Dimension gibt an welche Gewichtsmatrix gemeint ist
					for (int j=0;j<structure[i]+1;j++){ // zweite Dimension ist so gro� wie die "von" - Schicht +bias
						int k=0;
						while(k<structure[i+1]){
						//for (int k=0;k<structure[i+1];k++){ //die dritte Dimension ist so gro� wie die "zu" - Schicht
							string=reader.readLine();
							if (string.startsWith("#")||string.trim().equals("")) continue;
							v=Double.valueOf(string);
							weights[i].set(j,k,v);
							k=k+1;
						}
					}
				}
			}
			if(!exit){
				logger.info("loaded successfully: "+filename);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			logger.warning("Could not find file: "+filename);
		} catch (IOException e) {
			logger.warning("IOException: "+e.getMessage());
		}

	}




}


