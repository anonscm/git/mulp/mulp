package de.tarent.mulp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import de.tarent.mulp.config.Config;
import de.tarent.mulp.config.NetProperties;

public class Starter {

	protected final static Logger logger = Logger.getLogger(Starter.class.getName());
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		//initialize the configuration
		NetProperties props=null;
		try {
		if (args.length==0) {
		
				props= new NetProperties("src/main/config/mlp.properties");

			} else {
				props= new NetProperties(args[0]);
			}
		} catch (FileNotFoundException e) {
			logger.info("Could not find ConfigFile: "+e.getMessage());
			logger.info("switch to default config");
			props= new NetProperties();
		} catch (IOException e) {
			logger.info("IOException while trying to obtain ConfigFile: "+e.getMessage());
			logger.info("switch to default config");
			props= new NetProperties();
		} 
		logger.info("register Properties at Configclass");
		Config.getInstance().registerConfig(props);

	
		//TODO: handle the case of undefined properties
		
		//make a new MLP with config params
		int one = Config.getInstance().getLayerOne();
		int two = Config.getInstance().getLayerTwo();
		int three = Config.getInstance().getLayerThree();
				
		MLP mlp = new MLP(one,two,three);
		mlp.setHiddenLearningRate(Config.getInstance().getLearningRateHidden());
		mlp.setOutputLearningRate(Config.getInstance().getLearningRateOutput());
		
		//obtain the epoche file:
		Epoch epoch = new Epoch();
		File epochFile = Config.getInstance().getEpochFile();
		epoch.read(epochFile);
	
		//File file = new File("/src/main/resources/faults");
		File faultFile = Config.getInstance().getFaultFile();
		BufferedWriter writer;
		try {
			faultFile.createNewFile();
			writer=new BufferedWriter(new FileWriter(faultFile));
			writer.flush();
		} catch (IOException e1) {
			logger.info("could not write fault File: "+faultFile.getPath()+" , "+e1.getMessage());
			faultFile=null;
			writer=null;
		}
	
		int trainingsteps = Config.getInstance().getTrainingSteps();
		boolean verbose =Config.getInstance().getVerbose();
		boolean printfaults =Config.getInstance().getPrintFault();
		
		//*********TRAINING*****************
		mlp.runTraining(epoch, trainingsteps, verbose, printfaults, writer);
		
		if (writer!=null)
			try {
				writer.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	
}
