package de.tarent.mulp;

import java.util.ArrayList;
import java.util.List;

public class Utils {
	
	private Utils(){
		
	}
	
//	füllt eine Liste mit Nullen auf, damit sie die richtige Größe erhält
	public static void initialize(List<Double> list, int size, double value){
		if (list == null) list = new ArrayList<Double>(); 
		for (int i=0;i<size;i++) list.add(value);		
	}
	
	public static void printList(List<Double> list){
		System.out.print("(");
		for (int i=0;i<list.size();i++){
			System.out.print(list.get(i));
			if (i!=list.size()-1) System.out.print(" , ");
		}
		System.out.println(")");
	}

}
