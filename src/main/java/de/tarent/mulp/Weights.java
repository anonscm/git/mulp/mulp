package de.tarent.mulp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Weights {
	
	private List<List<Double>> weights;
	private int rowCount;
	private int colCount;
	public Weights(int rowCount, int colCount){
		weights= new ArrayList<List<Double>>(rowCount);
		for (int i=0;i<rowCount;i++){
			weights.add(new ArrayList<Double>());
			for (int j=0;j<colCount;j++){
				weights.get(i).add(new Double(0));
			}
		}
		this.rowCount=rowCount;
		this.colCount=colCount;
		
	}
	
	public void set(int row, int col, double value){
		List<Double> list = weights.get(row);
		list.set(col, value);
		
	}
	
	public double get(int row, int col){
		return weights.get(row).get(col);
	}
	
	public void printWeights(){
		System.out.println("Columns: "+colCount+" Rows: "+rowCount);
		for (List<Double> col:weights){
			System.out.print("|");
			for(Double dub:col){
				System.out.print(" "+dub+" | ");
			}
			System.out.println();
		}
	}
	
	public void initializeWithRandomNumber(){
		Random rand = new Random();
		double value;
		for (int i=0;i<rowCount;i++){
			for (int j=0;j<colCount;j++){
				value =rand.nextDouble();
				value=value/5-0.1;
				//System.out.println(value);
				set(i, j, value);
			}
		}
	}
	
	public void initializeWithValue(double value){
		for (int i=0;i<rowCount;i++){
			for (int j=0;j<colCount;j++){
				set(i, j, value);
			}
		}
	}

	public int getColCount() {
		return colCount;
	}

	public void setColCount(int colCount) {
		this.colCount = colCount;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	

}
