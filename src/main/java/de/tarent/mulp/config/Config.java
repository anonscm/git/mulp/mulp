package de.tarent.mulp.config;

import java.io.File;
import java.util.Properties;
import java.util.logging.Logger;

public class Config {

	protected final static Logger logger = Logger.getLogger(Config.class.getName());

	private static Config instance;
	private Properties properties;
	private Properties defaultProps = new NetProperties();

	private int layer_one;
	private int layer_two;
	private int layer_three;

	private double learning_rate_hidden;
	private double learning_rate_output;

	private boolean verbose;
	private boolean print_fault;
	private int training_steps;
	private File fault_file;
	private  File epoch_file;

	protected Config(){
		//TODO
	}

	public static Config getInstance(){
		if (instance==null) instance = new Config();
		return instance;
	}

	public void registerConfig(Properties props){
		this.properties=props;
		validateConfiguration();
	}

	public String getProperty(String key){
		if (properties!=null){
			return properties.getProperty(key);
		} else {
			logger.warning("no properties configured!");
			return null;
		}
	}

	private void validateConfiguration(){
		//mandatory	
		try{
			layer_one = Integer.parseInt(properties.getProperty(NetProperties.LAYER_ONE));
			layer_two = Integer.parseInt(properties.getProperty(NetProperties.LAYER_TWO));
			layer_three = Integer.parseInt(properties.getProperty(NetProperties.LAYER_THREE));
		} catch (NumberFormatException e){
			logger.warning("the structure of the neural network is not properly configured. Please specify the elements LAYER_ONE, LAYER_TWO and LAYER_THREE in the config file (valid integer values)");
			throw new RuntimeException("mandatory config elements are missing");
		}

		String epochFileName = properties.getProperty(NetProperties.EPOCH_FILE);
		epoch_file = new File(epochFileName);
		if (!epoch_file.canRead()){
			logger.warning("The epoche file is not properly configured. Please check if the path points to a valid epoche file");
			throw new RuntimeException("mandatory config elements are missing");
		}

		//the following elements can be filled with defaults (if not specified)
		try{
			training_steps = Integer.parseInt(properties.getProperty(NetProperties.TRAINING_STEPS));
		} catch (NumberFormatException e1){
			training_steps = Integer.parseInt(defaultProps.getProperty(NetProperties.TRAINING_STEPS));
			logger.warning("TRAINING_STEPS not properly configured, switching to default config: "+training_steps);
		}

		//TODO: do not rely on catching nullpointers...
		try{
			learning_rate_hidden = Double.valueOf(properties.getProperty(NetProperties.LEARNING_RATE_HIDDEN));
			learning_rate_output = Double.valueOf(properties.getProperty(NetProperties.LEARNING_RATE_OUTPUT));
		} catch (NullPointerException e2){
			learning_rate_hidden = Double.valueOf(defaultProps.getProperty(NetProperties.LEARNING_RATE_HIDDEN));
			learning_rate_output = Double.valueOf(defaultProps.getProperty(NetProperties.LEARNING_RATE_OUTPUT));
			logger.warning("learning rates not properly configured, switching to default config: "+learning_rate_hidden+", "+learning_rate_output);
		} catch (NumberFormatException e5){
			learning_rate_hidden = Double.valueOf(defaultProps.getProperty(NetProperties.LEARNING_RATE_HIDDEN));
			learning_rate_output = Double.valueOf(defaultProps.getProperty(NetProperties.LEARNING_RATE_OUTPUT));
			logger.warning("learning rates not properly configured, switching to default config: "+learning_rate_hidden+", "+learning_rate_output);
		}		

		verbose =Boolean.parseBoolean(properties.getProperty(NetProperties.VERBOSE));
		print_fault = Boolean.parseBoolean(properties.getProperty(NetProperties.PRINT_FAULT));

		//if print_fault is false, no fault_file is needed
		if (print_fault){
			String faultFileName = properties.getProperty(NetProperties.FAULT_FILE);
			fault_file = new File(faultFileName);
			if (!fault_file.canRead()){
				logger.warning("fault file not specified or not readable, printing to standard out");
			}	
		} else fault_file = null;

	}	

	public int getLayerOne() {
		return layer_one;
	}

	public int getLayerTwo() {
		return layer_two;
	}

	public int getLayerThree() {
		return layer_three;
	}

	public boolean getVerbose() {
		return verbose;
	}

	public boolean getPrintFault() {
		return print_fault;
	}

	public int getTrainingSteps() {
		return training_steps;
	}

	public File getFaultFile() {
		return fault_file;
	}

	public File getEpochFile() {
		return epoch_file;
	}

	public double getLearningRateHidden() {
		return learning_rate_hidden;
	}

	public double getLearningRateOutput() {
		return learning_rate_output;
	}
	
	
	
}
