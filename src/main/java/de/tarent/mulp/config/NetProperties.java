package de.tarent.mulp.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

public class NetProperties extends Properties{
	
	protected final static Logger logger = Logger.getLogger(NetProperties.class.getName());
	
	public static final String LAYER_ONE="LAYER_ONE";
	public static final String LAYER_TWO="LAYER_TWO";
	public static final String LAYER_THREE="LAYER_THREE";
	public static final String VERBOSE="VERBOSE";
	public static final String PRINT_FAULT="PRINT_FAULT";
	public static final String TRAINING_STEPS="TRAINING_STEPS";
	public static final String FAULT_FILE="FAULT_FILE";
	public static final String EPOCH_FILE="EPOCH_FILE";
	public static final String LEARNING_RATE_HIDDEN="LEARNING_RATE_HIDDEN";
	public static final String LEARNING_RATE_OUTPUT="LEARNING_RATE_OUTPUT";
		
	public NetProperties(String configFileName) throws FileNotFoundException, IOException{
		File configFile = new File(configFileName);
		load(new FileInputStream(configFile));
	}
	
	public NetProperties(){
		setProperty(LAYER_ONE, "2");
		setProperty(LAYER_TWO, "8");
		setProperty(LAYER_THREE, "1");
		setProperty(VERBOSE, "false");
		setProperty(PRINT_FAULT, "false");
		setProperty(TRAINING_STEPS, "100");
		setProperty(LEARNING_RATE_HIDDEN, "0.01");
		setProperty(LEARNING_RATE_OUTPUT, "0.1");
	}
}
