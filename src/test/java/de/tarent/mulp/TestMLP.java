package de.tarent.mulp;

import java.util.ArrayList;
import java.util.List;

import de.tarent.mulp.MLP;
import de.tarent.mulp.Utils;
import de.tarent.mulp.Weights;

import junit.framework.TestCase;

public class TestMLP extends TestCase {
	
	public void testSimpleMLP(){
		System.out.println("SimpleMLP");
		List<Double> input = new ArrayList<Double>();
		input.add(1.0);
		
		
		MLP mlp = new MLP(1,1,1);
		Weights[] weightsarray = mlp.getWeights();
		weightsarray[0].initializeWithValue(1);
		weightsarray[1].initializeWithValue(1);
		
		
		List<Double> output = mlp.forward(input);
		Utils.printList(output);
		
		
	}
	
	public void testMLPForward(){
		System.out.println("Random MLP");
		List<Double> input = new ArrayList<Double>();
		input.add(1.0);
		input.add(2.0);
		
		MLP mlp = new MLP(2,2,2);
		Weights[] weightsarray = mlp.getWeights();
		weightsarray[0].initializeWithRandomNumber();
		weightsarray[1].initializeWithRandomNumber();
		
		
		List<Double> output = mlp.forward(input);
		Utils.printList(output);
		
		
	}
	
	

}
