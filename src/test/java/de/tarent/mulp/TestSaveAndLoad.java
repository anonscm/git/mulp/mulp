package de.tarent.mulp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.tarent.mulp.MLP;

import junit.framework.TestCase;

public class TestSaveAndLoad extends TestCase {

	private boolean printToStandardOut=false;

	public void testSaveMlp(){
		MLP mlp = new MLP(2,3,4);
	
		mlp.getWeights()[0].initializeWithValue(0.5);
		mlp.getWeights()[1].initializeWithValue(0.3);
	
		String saveTo="src/test/resources/initialized.mlp";
		try {
			mlp.saveMlp(saveTo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testLoadMlp(){
		String loadFrom="src/test/resources/initialized.mlp";
		MLP mlp = new MLP(2,3,4);
		mlp.loadMlp(loadFrom);
		if (printToStandardOut){
			mlp.getWeights()[0].printWeights();
			mlp.getWeights()[1].printWeights();
		}

	}
	
//	removed temporarily because of tempfile problem
//	public void testSaveAndLoadMLP(){
//		File tempFile = null;
//		try {
//			MLP mlp = new MLP(2,8,5);
//
//			tempFile = File.createTempFile("test", "mlp");
//			mlp.saveMlp(tempFile.getName());
//
//			MLP newMLP = new MLP(2,8,5);
//			newMLP.loadMlp(tempFile.getName());
//			tempFile.delete();
//		
//			for (int i=0;i<newMLP.getWeights()[0].getColCount();i++){
//				for (int j=0;j<newMLP.getWeights()[0].getRowCount();j++){
//					assertEquals(mlp.getWeights()[0].get(j, i),newMLP.getWeights()[0].get(j, i));
//				}
//			}
//	
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			tempFile.delete();
//		}
//
//	}

	public void testSuperAND(){
		MLP mlp = new MLP(2,8,1);
//		this is how the output below was generated:

//		Epoch e = new Epoch();
//		e.read("src/main/resources/and.epo");
//		mlp.runTraining(e, 10000, false, false, null);
//		List<Double> input = new ArrayList<Double>();
//		input.add(0.9);
//		input.add(-0.9);
//		double output = (mlp.compute(input)).get(0);
//		System.out.println("output:" +output);


		mlp.loadMlp("src/test/resources/super-and.mlp");
		List<Double> input = new ArrayList<Double>();
		input.add(0.9);
		input.add(-0.9);
		double output = (mlp.compute(input)).get(0);
		assertEquals(-0.898985402895568, output);
	}

}
