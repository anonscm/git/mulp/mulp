package de.tarent.mulp;

import de.tarent.mulp.Weights;
import junit.framework.TestCase;

public class TestWeights extends TestCase {

	protected void setUp() throws Exception {
	}

	protected void tearDown() throws Exception {
	}
	
	public void testInitializeWeights(){
		int cols=7;
		int rows=3;
		Weights weights = new Weights(rows,cols);
		
		assertEquals(7, weights.getColCount());
		assertEquals(3, weights.getRowCount());
	}
	
	public void testSetAndGet(){
		Weights weights= new Weights(15,3);
		
		weights.set(7,2, 15.37);
		assertEquals(15.37,weights.get(7,2));
		
		weights.set(14, 2, 2);
		assertEquals(2.0,weights.get(14,2));
		
		
		
	}
	
	
	
	
	

}
